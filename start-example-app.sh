#!/bin/sh

if [ -z "$SERVICE_NAME" ]; then
    echo "Not making any change to service name"
else
    echo "Overriding service name to $SERVICE_NAME"
    cat /usr/share/nginx/html/config.json | jq ".service.name=\"$SERVICE_NAME\"" > /tmp/config.json
    mv /tmp/config.json /usr/share/nginx/html/config.json
fi

if [ -z "$API_HOST" ]; then
    echo "Not making any change to api host"
else
    echo "Overriding api host to $API_HOST"
    cat /usr/share/nginx/html/config.json | jq ".service.apiHost=\"$API_HOST\"" > /tmp/config.json
    mv /tmp/config.json /usr/share/nginx/html/config.json
fi

if [ -z "$CLIENT_ID" ]; then
    echo "Not making any change to client id"
else
    echo "Overriding client id to $CLIENT_ID"
    cat /usr/share/nginx/html/config.json | jq ".clientId=\"$CLIENT_ID\"" > /tmp/config.json
    mv /tmp/config.json /usr/share/nginx/html/config.json
fi

if [ -z "$AUTH_ENDPOINT" ]; then
    echo "Not making any change to the authorization endpoint"
else
    echo "Overriding auth endpoint to $AUTH_ENDPOINT"
    cat /usr/share/nginx/html/config.json | jq ".service.endpoints.authorization_endpoint=\"$AUTH_ENDPOINT\"" > /tmp/config.json
    mv /tmp/config.json /usr/share/nginx/html/config.json
fi

if [ -z "$TOKEN_ENDPOINT" ]; then
    echo "Not making any change to the token endpoint"
else
    echo "Overriding token endpoint to $TOKEN_ENDPOINT"
    cat /usr/share/nginx/html/config.json | jq ".service.endpoints.token_endpoint=\"$AUTH_ENDPOINT\"" > /tmp/config.json
    mv /tmp/config.json /usr/share/nginx/html/config.json
fi



exec "$@"