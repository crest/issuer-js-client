import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Auth from './Auth';
import * as serviceWorker from './serviceWorker';
import OauthClientFactory from '../../../src/index';

let client;

async function bootstrap() {
    const config = await fetch('/config.json').then(r => r.json());
    client = new OauthClientFactory({
        autoRedirect: true,
        redirectUri: window.location.origin,
        services: [config.service],
        clientId: config.clientId,
    });

    client
        .bootstrap()
        .then(() => startReactApp(client, config))
        .catch(e => {
            const component = e.authenticationRequired ? (
                <Auth
                    client={client}
                    onAuth={() => startReactApp(client, config)}
                />
            ) : (
                <ErrorDisplay error={e} />
            );

            ReactDOM.render(component, document.getElementById('root'));
        });
}

function ErrorDisplay({ error }) {
    return (
        <div id="auth-container" className="error">
            An error has occurred!
            <br />
            Message: {error.message}
            <pre>{error.stack}</pre>
        </div>
    );
}

function startReactApp(client, config) {
    ReactDOM.render(
        <App oauthClient={client} config={config} />,
        document.getElementById('root'),
    );
}

bootstrap().catch(console.error);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
