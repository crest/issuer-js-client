import React, { useEffect, useState, useMemo } from 'react';
import logo from './logo.svg';
import './App.css';
import ApiContext from './ApiContext';
import WhoAmI from './WhoAmI';
import Modal from 'react-bootstrap/es/Modal';
import Button from 'react-bootstrap/es/Button';

let authCb = null;

function App({ oauthClient, config }) {
    const clients = useMemo(
        () => ({
            api: oauthClient.getServiceEndpoint(config.service.name),
        }),
        [oauthClient],
    );

    const [authNeeded, setAuthNeeded] = useState(false);
    const [authenticated, setAuthenticated] = useState(true);
    const [logoutUrl, setLogoutUrl] = useState(null);

    useEffect(() => {
        oauthClient.onAuthenticationRequired(cb => {
            authCb = cb;
            console.log('Set auth needed');
            setAuthNeeded(true);
        });

        oauthClient.onLogout(config => {
            setAuthenticated(false);
            setLogoutUrl(config.redirect_url);
        });

        oauthClient.config.tokenStore.on('accessToken.obtained', () => {
            setAuthNeeded(false);
        });
    }, [oauthClient]);

    const onAuthClick = () => {
        if (authCb) authCb();
    };

    const logout = () => {
        oauthClient.logout();
    };

    return (
        <ApiContext.Provider value={clients}>
            <div className="App">
                <header className="App-header">
                    {authenticated && (
                        <>
                            <img src={logo} className="App-logo" alt="logo" />
                            <p>
                                You now have an app with a configured client!!!
                            </p>

                            <Button variant={'danger'} onClick={logout}>
                                Logout
                            </Button>

                            <hr />

                            <WhoAmI />
                        </>
                    )}
                </header>

                <Modal show={!authenticated} backdrop={'static'} size={'lg'}>
                    <Modal.Header>
                        <Modal.Title>Logout Completed</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p>
                            You have been logged out of this example app.
                            However, you may still be logged into the single
                            sign-on. To log out of single sign-on, click the
                            button below.
                        </p>
                        <p>
                            <Button href={logoutUrl} variant={'primary'}>
                                Log out of Single Sign-On
                            </Button>
                        </p>
                    </Modal.Body>
                </Modal>

                <Modal show={authNeeded} backdrop={'static'} size={'lg'}>
                    <Modal.Header>
                        <Modal.Title>Authentication Required</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <p>
                            Your login session has timed out. To continue to use
                            this app, you will need to renew it.
                        </p>
                        <p>
                            After clicking the button below, you will see a
                            pop-up that will re-establish your session.
                        </p>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="primary" onClick={onAuthClick}>
                            Authenticate
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        </ApiContext.Provider>
    );
}

export default App;
