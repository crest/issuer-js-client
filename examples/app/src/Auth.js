import React, { useState } from 'react';
import './Auth.css';

function Auth(props) {
    const { client, onAuth } = props;

    const [authenticating, setAuthenticating] = useState(false);
    const [authError, setAuthError] = useState(null);

    const startAuth = () => {
        setAuthenticating(true);
        client
            .ensureAuthenticated()
            .then(onAuth)
            .catch(err => {
                setAuthError(err);
                setAuthenticating(false);
            });
    };

    return (
        <div id="auth-container">
            <AuthDisplay
                authenticating={authenticating}
                authError={authError}
                onAuthClick={startAuth}
            />
        </div>
    );
}

function AuthDisplay(props) {
    const { authenticating, authError, onAuthClick } = props;

    if (authenticating)
        return (
            <p>
                <i className="fa fa-spin fa-circle-o-notch" />
                &nbsp;Authenticating...
            </p>
        );

    return (
        <React.Fragment>
            {authError && <div className="error">{authError.message}</div>}
            <p>
                It looks like you need to login. Click the button below to get
                started!
            </p>
            <p>
                <button onClick={onAuthClick}>Start authenticating</button>
            </p>
        </React.Fragment>
    );
}

export default Auth;
