import React, { useContext, useState } from 'react';
import ApiContext from './ApiContext';
import Button from 'react-bootstrap/Button';

function WhoAmI() {
    const { api } = useContext(ApiContext);
    const [user, setUser] = useState(null);
    const [error, setError] = useState(null);

    const makeRequest = () => {
        api.get('/whoami')
            .then(response => setUser(response.data))
            .catch(err => setError(err));
    };

    return (
        <React.Fragment>
            <Button variant={'primary'} onClick={makeRequest}>
                Who am I?
            </Button>

            {error && (
                <pre
                    style={{
                        textAlign: 'left',
                        fontSize: '16px',
                        color: 'red',
                    }}
                >
                    {error.stack}
                </pre>
            )}

            <pre
                style={{ textAlign: 'left', fontSize: '16px', color: 'white' }}
            >
                {JSON.stringify(user, null, 2)}
            </pre>
        </React.Fragment>
    );
}

export default WhoAmI;
