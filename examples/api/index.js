const express = require('express');
const app = express();
const cors = require('cors');
const expressJwt = require('express-jwt');
const jwt = require('jsonwebtoken');
const https = require('https');

const port = 3000;

const certs = {};

const jwtMiddleware = expressJwt({
    issuer: 'edu.vt.it.devcom.oauth.issuer',
    secret: (req, header, payload, cb) => {
        if (certs[header.x5t]) return cb(null, certs[header.x5t]);

        if (header.x5c && header.x5c.length > 0) {
            const data = `-----BEGIN CERTIFICATE-----\n${header.x5c[0]
                .match(/.{0,64}/g)
                .join('')}\n-----END CERTIFICATE-----`;
            return cb(null, data);
        }

        https
            .get(
                'https://issuer.local.summit.cloud.vt.edu' + header.x5u,
                { rejectUnauthorized: false },
                res => {
                    let data = '';
                    res.on('data', chunk => (data += chunk));
                    res.on('end', () => {
                        console.log('Retrieved a new cert!\n', data);
                        certs[header.x5t] = data;
                        cb(null, data);
                    });
                },
            )
            .on('error', console.error);
    },
});

const corsOptions = {
    origin: 'https://app.local.summit.cloud.vt.edu',
    credentials: true,
};

app.use(cors(corsOptions));

app.get('/.well-known/openid-configuration', (req, res) => {
    res.send({
        authorization_endpoint:
            'https://issuer.localhost.vt.edu/oauth/authorize',
        token_endpoint: 'https://issuer.localhost.vt.edu/oauth/token',
    });
});

app.get('/whoami', jwtMiddleware, (req, res) => {
    res.send(req.user);
});

app.use(function(err, req, res, next) {
    console.log(err);
    if (err.name === 'UnauthorizedError') {
        return res.status(401).send({
            issuer: {
                host: 'issuer.localhost',
            },
            service: 'mock-api',
        });
    }
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
