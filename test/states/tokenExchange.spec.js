import state from '../../src/states/tokenExchange';
const fetchMock = require('fetch-mock');

let clientConfig, oauthState, machine, fetchClient;

beforeEach(() => {
    oauthState = {
        pkce: { code: 'pkce-code' },
        token_endpoint: 'https://token-endpoint',
    };

    clientConfig = {
        clientId: 'CLIENT-ID',
        oauthState: {
            get: name => oauthState[name],
        },
        tokenStore: {
            getAuthorizationCode: () => 'CODE',
            setRefreshToken: jest.fn(),
            setAccessToken: jest.fn(),
        },
    };

    machine = {
        advanceTo: jest.fn(),
    };
});

afterEach(() => {
    fetchMock.reset();
});

test('has the expected name', () => {
    expect(state.name).toEqual('TOKEN_EXCHANGE');
});

test('performs the token exchange', async () => {
    fetchMock.mock('https://token-endpoint', {
        status: 200,
        body: {
            access_token: 'ACCESS_TOKEN',
            refresh_token: 'REFRESH_TOKEN',
            expires_in: 60,
        },
    });

    await state.onEnter(clientConfig, machine);

    const lastFetchOptions = fetchMock.lastOptions();
    expect(lastFetchOptions.method).toEqual('POST');
    expect(lastFetchOptions.credentials).toEqual('include');
    expect(lastFetchOptions.mode).toEqual('cors');
    expect(lastFetchOptions.headers.accept).toEqual('application/json');
    expect(lastFetchOptions.headers['content-type']).toEqual(
        'application/json',
    );
    expect(lastFetchOptions.body).toEqual(
        JSON.stringify({
            grant_type: 'authorization_code',
            client_id: 'CLIENT-ID',
            code: 'CODE',
            code_verifier: 'pkce-code',
        }),
    );

    expect(clientConfig.tokenStore.setRefreshToken).toBeCalled();
    expect(clientConfig.tokenStore.setRefreshToken).toBeCalledWith(
        'REFRESH_TOKEN',
    );

    expect(clientConfig.tokenStore.setAccessToken).toBeCalled();
    expect(clientConfig.tokenStore.setAccessToken).toBeCalledWith(
        'ACCESS_TOKEN',
        60,
    );

    expect(machine.advanceTo).toBeCalled();
    expect(machine.advanceTo).toBeCalledWith('CONFIGURED');
});
