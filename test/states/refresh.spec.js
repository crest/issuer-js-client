import state from '../../src/states/refresh';
const fetchMock = require('fetch-mock');

let clientConfig, oauthState, machine;

beforeEach(() => {
    oauthState = {
        pkce: { code: 'pkce-code' },
        token_endpoint: 'https://token-endpoint',
    };

    clientConfig = {
        clientId: 'CLIENT-ID',
        oauthState: {
            get: name => oauthState[name],
        },
        tokenStore: {
            getRefreshToken: () => 'REFRESH-TOKEN',
            invalidateRefreshToken: jest.fn(),
            setRefreshToken: jest.fn(),
            setAccessToken: jest.fn(),
        },
    };

    machine = {
        advanceTo: jest.fn(),
    };
});

afterEach(() => {
    fetchMock.reset();
});

test('has the expected name', () => {
    expect(state.name).toEqual('REFRESH');
});

test('performs the refresh on enter', async () => {
    fetchMock.mock('https://token-endpoint', {
        status: 200,
        body: {
            access_token: 'ACCESS_TOKEN',
            refresh_token: 'REFRESH_TOKEN',
            expires_in: 60,
        },
    });

    await state.onEnter(clientConfig, machine);

    const lastFetchOptions = fetchMock.lastOptions();
    expect(lastFetchOptions.method).toEqual('POST');
    expect(lastFetchOptions.credentials).toEqual('include');
    expect(lastFetchOptions.mode).toEqual('cors');
    expect(lastFetchOptions.headers.accept).toEqual('application/json');
    expect(lastFetchOptions.headers['content-type']).toEqual(
        'application/json',
    );
    expect(lastFetchOptions.body).toEqual(
        JSON.stringify({
            grant_type: 'refresh_token',
            refresh_token: 'REFRESH-TOKEN',
        }),
    );

    expect(clientConfig.tokenStore.setRefreshToken).toBeCalled();
    expect(clientConfig.tokenStore.setRefreshToken).toBeCalledWith(
        'REFRESH_TOKEN',
    );

    expect(clientConfig.tokenStore.setAccessToken).toBeCalled();
    expect(clientConfig.tokenStore.setAccessToken).toBeCalledWith(
        'ACCESS_TOKEN',
        60,
    );

    expect(machine.advanceTo).toBeCalled();
    expect(machine.advanceTo).toBeCalledWith('CONFIGURED');
});

test('handles 401 by invalidating the refresh token and going to AUTHENTICATE', async () => {
    fetchMock.mock('https://token-endpoint', { status: 401, body: {} });

    await state.onEnter(clientConfig, machine);

    expect(clientConfig.tokenStore.invalidateRefreshToken).toBeCalled();
    expect(clientConfig.tokenStore.setRefreshToken).not.toBeCalled();
    expect(clientConfig.tokenStore.setAccessToken).not.toBeCalled();

    expect(machine.advanceTo).toBeCalled();
    expect(machine.advanceTo).toBeCalledWith('AUTHENTICATE');
});

test('throws an error on non-200/401 response', async () => {
    fetchMock.mock('https://token-endpoint', { status: 500, body: {} });

    try {
        await state.onEnter(clientConfig, machine);
        throw new Error('Should have thrown');
    } catch (e) {
        expect(e.message).toContain('Error during token refresh');
        expect(clientConfig.tokenStore.invalidateRefreshToken).not.toBeCalled();
        expect(clientConfig.tokenStore.setRefreshToken).not.toBeCalled();
        expect(clientConfig.tokenStore.setAccessToken).not.toBeCalled();
    }
});
