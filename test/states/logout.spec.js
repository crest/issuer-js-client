import state from '../../src/states/logout';
const fetchMock = require('fetch-mock');

const REFRESH_TOKEN = 'some-bogus-refresh-token';
const TOKEN_ENDPOINT = 'https://token-issuer.localhost/oauth/token';

let config, machine;

beforeEach(() => {
    config = {
        tokenStore: {
            getRefreshToken: jest.fn().mockImplementation(() => REFRESH_TOKEN),
            clearTokens: jest.fn(),
        },
        oauthState: {
            get: () => TOKEN_ENDPOINT,
        },
    };

    machine = {
        advanceTo: jest.fn(),
    };
});

afterEach(() => {
    fetchMock.reset();
});

test('it has the right name', () => {
    expect(state.name).toBe('LOGOUT');
});

test('onEnter when logout response unsuccessful', async () => {
    fetchMock.mock(TOKEN_ENDPOINT, 500);

    try {
        await state.onEnter(config, machine);
        fail('Should have thrown');
    } catch (err) {
        expect(err.message).toContain(
            'Logout failed due to unexpected response: 500',
        );
    }
});

test('onEnter when logout successful', async () => {
    const data = { redirect_uri: '/logout' };
    fetchMock.mock(TOKEN_ENDPOINT, data);

    await state.onEnter(config, machine);

    expect(config.tokenStore.clearTokens).toHaveBeenCalled();
    expect(config.tokenStore.clearTokens).toHaveBeenCalledWith({
        ...data,
        redirect_url: 'https://token-issuer.localhost/logout',
    });
});
