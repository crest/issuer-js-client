import TokenStore from '../src/tokenStore';

const TOKEN = 'an-access-token';
let tokenStore;

beforeEach(() => {
    tokenStore = new TokenStore();
});

test('constructs correctly', () => {
    expect(tokenStore.getRefreshToken()).toEqual(null);
    expect(tokenStore.hasRefreshToken()).toEqual(false);
    expect(tokenStore.hasAccessToken()).toEqual(false);
});

test('sets access token correctly with no expiration', () => {
    tokenStore.setAccessToken(TOKEN, null);

    expect(tokenStore.hasAccessToken()).toEqual(true);
    expect(tokenStore.getAccessToken()).toEqual(TOKEN);
    expect(tokenStore.getAccessTokenExpiration()).toEqual(null);
});

test('sets access token correctly when given expiration', () => {
    const expirationSeconds = 100;

    tokenStore.setAccessToken(TOKEN, expirationSeconds);

    expect(tokenStore.hasAccessToken()).toEqual(true);
    expect(tokenStore.getAccessToken()).toEqual(TOKEN);

    const expiration = tokenStore.getAccessTokenExpiration();
    const now = Date.now();
    expect(expiration).not.toEqual(null);
    expect(expiration).toBeLessThan(now + expirationSeconds * 1000 + 100);
    expect(expiration).toBeGreaterThan(now + expirationSeconds * 1000 - 100);
});

test('sets authorization code correctly', () => {
    tokenStore.setAuthorizationCode(TOKEN);
    expect(tokenStore.getAuthorizationCode()).toEqual(TOKEN);
});

test('sets refresh token correctly', () => {
    tokenStore.setRefreshToken(TOKEN, null);

    expect(tokenStore.hasRefreshToken()).toEqual(true);
    expect(tokenStore.getRefreshToken()).toEqual(TOKEN);
});

test('clears tokens correctly', () => {
    tokenStore.setAccessToken(TOKEN);
    tokenStore.setRefreshToken(TOKEN);

    tokenStore.clearTokens();

    expect(tokenStore.hasAccessToken()).toEqual(false);
    expect(tokenStore.hasRefreshToken()).toEqual(false);
    expect(tokenStore.getAccessToken()).toEqual(null);
    expect(tokenStore.getRefreshToken()).toEqual(null);
});

test('invalidates refresh tokens and broadcasts an event', async () => {
    tokenStore.setRefreshToken(TOKEN);
    tokenStore.setAccessToken(TOKEN);

    let eventReceived = false,
        eventData = null;
    tokenStore.once('refreshToken.invalidated', data => {
        eventReceived = true;
        eventData = data;
    });

    const data = { id: 123 };
    tokenStore.invalidateRefreshToken(data);
    expect(tokenStore.hasRefreshToken()).toEqual(false);
    expect(tokenStore.hasAccessToken()).toEqual(true);

    return new Promise((acc, rej) => {
        setTimeout(() => {
            try {
                expect(eventReceived).toEqual(true);
                expect(eventData).toEqual(data);
                acc();
            } catch (e) {
                rej(e);
            }
        });
    });
});

test('invalidates access tokens and broadcasts an event', async () => {
    tokenStore.setRefreshToken(TOKEN);
    tokenStore.setAccessToken(TOKEN);

    let eventReceived = false;
    tokenStore.once('accessToken.invalidated', () => (eventReceived = true));

    tokenStore.invalidateAccessToken();
    expect(tokenStore.hasAccessToken()).toEqual(false);
    expect(tokenStore.hasRefreshToken()).toEqual(true);

    return new Promise((acc, rej) => {
        setTimeout(() => {
            try {
                expect(eventReceived).toEqual(true);
                acc();
            } catch (e) {
                rej(e);
            }
        });
    });
});
