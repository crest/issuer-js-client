import { getUrlParams } from './utils';

const STORAGE_KEY = 'oauth_state';

export class StateManager {
    constructor() {
        const urlParameters = getUrlParams();

        this.storedData = JSON.parse(
            sessionStorage.getItem(STORAGE_KEY) || '{}',
        );
        this.stateKey = urlParameters.state || StateManager.generateStateName();

        if (
            urlParameters.state !== undefined &&
            urlParameters.state !== this.storedData.stateKey
        ) {
            throw new Error(
                `We have a stored state value, but the provided one is different - ${
                    urlParameters.state
                } vs ${this.storedData.stateKey}`,
            );
        }

        this.put('stateKey', this.stateKey);
    }

    put(key, value) {
        this.storedData[key] = value;
        sessionStorage.setItem(STORAGE_KEY, JSON.stringify(this.storedData));
    }

    get(key) {
        return this.storedData[key];
    }

    static generateStateName() {
        const validChars =
            'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let array = new Uint8Array(Math.floor(Math.random() * 5) + 12);
        window.crypto.getRandomValues(array);
        array = array.map(x => validChars.charCodeAt(x % validChars.length));
        return String.fromCharCode.apply(null, array);
    }
}
