import states from './states';
import TokenStore from './tokenStore';
import PkceGenerator from './pkce';
import ServiceClient from './serviceClient';
import { StateManager } from './stateManager';

/**
 * Config options:
 * {
 *   clientId,
 *   services: [
 *     {
 *       name,
 *       apiHost,
 *       baseUrl?, // Base URL for all requests
 *       issuerUrl?, // one of either issuerUrl or endpoints must be provided
 *       endpoints?: { // If not provided, will load from ${issuerUrl}/.well-known/openid-configuration
 *         authorization_endpoint,
 *         token_endpoint,
 *       }
 *     }
 *   ],
 *   autoRedirect // Whether to automatically redirect during bootstrapping. Defaults to true
 * }
 */
// const DEFAULT_CONFIG = {
// { name, apiHost, authCheckResource, tokenTransformer }
// services : [],

// openIdConfig : { authorization_endpoint, token_endpoint }

// tokenStore: TokenStore,
// pkceGenerator: PkceGenerator,
// oauthState: {}
// };

class OauthClientFactory {
    constructor(config) {
        if (!config.services.length === 0)
            throw new Error(`No services defined`);

        this.config = Object.assign(
            {},
            {
                autoRedirect: true,
                onLogout: () => {},
                onAuthenticationRequired: () => {},
            },
            config,
            {
                tokenStore: new TokenStore(),
                pkceGenerator: new PkceGenerator(),
                oauthState: new StateManager(),
            },
        );

        this.machine = {
            advanceTo: async stateName => {
                this.changeState(stateName);
                return states[stateName].onEnter(this.config, this.machine);
            },
        };

        this.changeState = newState => {
            console.log(`Machine: ${this.state} --> ${newState}`);
            this.state = newState;
        };

        this.config.tokenStore.on('accessToken.invalidated', () =>
            this._refreshAccessToken(),
        );

        this.config.tokenStore.on('accessToken.obtained', () =>
            this.configureAccessTokenAutoRefresher(),
        );

        this.config.tokenStore.on('tokens.invalidated', config => {
            this.changeState('UNAUTHENTICATED');
            if (this.tokenRefreshTimer) clearTimeout(this.tokenRefreshTimer);
            this.config.onLogout(config);
        });

        this.state = 'BOOTSTRAP';
    }

    onAuthenticationRequired(cb) {
        this.config.onAuthenticationRequired = cb;
    }

    onLogout(cb) {
        this.config.onLogout = cb;
    }

    async logout() {
        this.changeState('LOGOUT');
        return states[this.state].onEnter(this.config, this.machine);
    }

    configureAccessTokenAutoRefresher() {
        if (this.tokenRefreshTimer) clearTimeout(this.tokenRefreshTimer);

        const expiration = this.config.tokenStore.getAccessTokenExpiration();
        if (!expiration) return;

        const timer = Math.floor((expiration - Date.now()) * 0.75);
        this.tokenRefreshTimer = setTimeout(() => {
            this._refreshAccessToken();
        }, timer);
    }

    bootstrap() {
        return states[this.state]
            .onEnter(this.config, this.machine)
            .then(() => {
                if (states[this.state].configured) {
                    return;
                }

                const error = new Error('Authentication required');
                error.authenticationRequired = true;
                throw error;
            });
    }

    async _refreshAccessToken() {
        if (!states[this.state].configured && this.state !== 'ERROR') {
            return console.trace(
                'Not changing states to refresh token because in state ' +
                    this.state,
            );
        }

        this.changeState('REFRESH');
        states[this.state]
            .onEnter(this.config, this.machine)
            .then(() => {
                if (states[this.state].configured) return;

                if (this.config.onAuthenticationRequired) {
                    this.config.onAuthenticationRequired(() =>
                        this.ensureAuthenticated(),
                    );
                }
            })
            .catch(e => {
                this.changeState('ERROR');
                console.log('Caught error', e);
            });
    }

    async ensureAuthenticated() {
        try {
            console.log('Calling ensure authenticated');
            return states[this.state].authenticate(this.config, this.machine);
        } catch (err) {
            console.log('Error while trying to ensure authentication', err);
        }
    }

    getServiceEndpoint(serviceName) {
        if (!states[this.state].configured)
            throw new Error(
                'Unable to get service endpoint when not fully configured',
            );

        const service = this.config.services.find(s => s.name === serviceName);
        if (service === undefined)
            throw new Error(`Unable to find service with name: ${serviceName}`);

        const baseUrl = service.baseUrl || `https://${service.apiHost}`;
        return new ServiceClient(baseUrl, this.config.tokenStore);
    }
}

export default OauthClientFactory;
