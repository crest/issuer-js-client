export function getUrlParams() {
    if (window.location.search === '') return {};

    return window.location.search
        .slice(1)
        .split('&')
        .map(p => p.split('='))
        .reduce(
            (acc, [name, val]) => Object.assign({}, acc, { [name]: val }),
            {},
        );
}
