import axios from 'axios';

export default class ServiceClient {
    constructor(baseUrl, tokenStore) {
        this.tokenStore = tokenStore;
        this.axiosClient = axios.create({
            baseURL: baseUrl,
        });

        this.axiosClient.interceptors.request.use(function(config) {
            if (tokenStore.hasAccessToken()) return config;

            return new Promise(acc => {
                tokenStore.once('accessToken.obtained', () => {
                    acc(config);
                });
            });
        });

        this.axiosClient.interceptors.request.use(config => {
            config.headers.common[
                'Authorization'
            ] = `Bearer ${tokenStore.getAccessToken()}`;
            return config;
        });

        this.axiosClient.interceptors.response.use(
            r => r,
            error => {
                if (!error.response
                    || !error.response.status
                    || error.response.status !== 401) {
                    return Promise.reject(error);
                }

                tokenStore.invalidateAccessToken();
                return new Promise((acc, rej) => {
                    tokenStore.once('accessToken.obtained', () => {
                        const config = error.config;

                        if (config.requestCount === undefined)
                            config.requestCount = 1;

                        if (config.requestCount === 3)
                            return rej(
                                new Error(
                                    'Auth error loop detected - received three back-to-back 401 responses',
                                    error,
                                ),
                            );

                        config.requestCount++;

                        config.headers[
                            'Authorization'
                        ] = `Bearer ${tokenStore.getAccessToken()}`;
                        this.axiosClient
                            .request(config)
                            .then(acc)
                            .catch(rej);
                    });
                });
            },
        );
    }

    getAccessToken() {
        return this.tokenStore.getAccessToken();
    }

    clearTokens(data) {
        this.tokenStore.clearTokens(data);
    }

    invalidateAccessToken() {
        this.tokenStore.invalidateAccessToken();
    }

    invalidateRefreshToken(data) {
        this.tokenStore.invalidateRefreshToken(data);
    }

    get(url, config) {
        return this.axiosClient.get(url, config);
    }

    post(url, data, config) {
        return this.axiosClient.post(url, data, config);
    }

    put(url, data, config) {
        return this.axiosClient.put(url, data, config);
    }

    del(url, config) {
        return this.axiosClient.delete(url, config);
    }

    request(config) {
        return this.axiosClient.request(config);
    }
}
