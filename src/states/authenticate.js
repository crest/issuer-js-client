async function onEnter(clientConfig, machine) {}

async function authenticate(clientConfig, machine) {
    const location = clientConfig.redirectUri || window.location.href;

    const pkceChallenge = await clientConfig.pkceGenerator.generateChallenge();

    clientConfig.oauthState.put('pkce', pkceChallenge);
    const stateKey = clientConfig.oauthState.get('stateKey');

    const paramsObj = {
        response_type: 'code',
        state: stateKey,
        client_id: clientConfig.clientId,
        code_challenge: pkceChallenge.challenge,
        code_challenge_method: pkceChallenge.method,
        redirect_uri: location,
    };

    const params = Object.keys(paramsObj)
        .map(k => k + '=' + encodeURIComponent(paramsObj[k]))
        .join('&');

    const popupWidth = Math.min(1000, window.outerWidth);
    const popupHeight = Math.min(600, window.outerHeight);
    const left = (window.outerWidth - popupWidth) / 2 + window.screenX;
    const top = (window.outerHeight - popupHeight) / 2 + window.screenY;

    let authWindow = window.open(
        `${clientConfig.oauthState.get('authorization_endpoint')}?${params}`,
        'auth',
        `height=${popupHeight},width=${popupWidth},top=${top},left=${left}`,
    );

    if (authWindow === null) {
        const error = new Error(
            'Popup blocker blocked reauthentication attempt',
        );
        error.type = 'PopupBlocked';
        throw error;
    }

    window.onunload = function() {
        authWindow.close();
    };

    return new Promise((acc, rej) => {
        const closePoller = setInterval(() => {
            if (authWindow.closed) {
                window[`completeOAuthLogin-${stateKey}`] = undefined;
                rej(new Error('Authentication request canceled'));
            }
        }, 200);

        window[`completeOAuthLogin-${stateKey}`] = authCode => {
            window[`completeOAuthLogin-${stateKey}`] = undefined;
            clearInterval(closePoller);

            authWindow.close();
            clientConfig.tokenStore.setAuthorizationCode(authCode);

            machine
                .advanceTo('TOKEN_EXCHANGE')
                .then(acc)
                .catch(rej);
        };
    });
}

export default {
    name: 'AUTHENTICATE',
    onEnter,
    authenticate,
};
