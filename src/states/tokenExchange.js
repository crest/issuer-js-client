async function onEnter(clientConfig, machine) {
    const body = JSON.stringify({
        grant_type: 'authorization_code',
        client_id: clientConfig.clientId,
        code: clientConfig.tokenStore.getAuthorizationCode(),
        code_verifier: clientConfig.oauthState.get('pkce').code,
    });

    const response = await fetch(
        clientConfig.oauthState.get('token_endpoint'),
        {
            method: 'POST',
            mode: 'cors',
            body,
            credentials: 'include',
            headers: {
                accept: 'application/json',
                'content-type': 'application/json',
            },
        },
    );

    const data = await response.json();

    if (response.status !== 200)
        throw new Error(
            `Error during token exchange: ${data.error} : ${
                data.error_description
            }`,
        );

    clientConfig.tokenStore.setRefreshToken(data.refresh_token);
    clientConfig.tokenStore.setAccessToken(data.access_token, data.expires_in);
    return machine.advanceTo('CONFIGURED');
}

export default {
    name: 'TOKEN_EXCHANGE',
    onEnter,
};
