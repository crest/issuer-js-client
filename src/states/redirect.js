async function onEnter(clientConfig, machine) {
    const location = clientConfig.redirectUri || window.location.href;

    const pkceChallenge = await clientConfig.pkceGenerator.generateChallenge();

    clientConfig.oauthState.put('pkce', pkceChallenge);
    const stateKey = clientConfig.oauthState.get('stateKey');

    const paramsObj = {
        response_type: 'code',
        state: stateKey,
        client_id: clientConfig.clientId,
        code_challenge: pkceChallenge.challenge,
        code_challenge_method: pkceChallenge.method,
        redirect_uri: location,
    };

    const params = Object.keys(paramsObj)
        .map(k => k + '=' + encodeURIComponent(paramsObj[k]))
        .join('&');

    clientConfig.oauthState.put(
        'route',
        window.location.href.replace(window.location.origin, ''),
    );
    window.location = `${clientConfig.oauthState.get(
        'authorization_endpoint',
    )}?${params}`;
}

export default {
    name: 'REDIRECT',
    onEnter,
};
