import BOOTSTRAP from './bootstrap';
import NOTIFY from './notify';
import REFRESH from './refresh';
import AUTHENTICATE from './authenticate';
import REDIRECT from './redirect';
import TOKEN_EXCHANGE from './tokenExchange';
import CONFIGURED from './configured';
import LOGOUT from './logout';
import UNAUTHENTICATED from './unauthenticated';
import ERROR from './error';

export default {
    BOOTSTRAP,
    NOTIFY,
    REFRESH,
    REDIRECT,
    AUTHENTICATE,
    TOKEN_EXCHANGE,
    CONFIGURED,
    LOGOUT,
    UNAUTHENTICATED,
    ERROR,
};
