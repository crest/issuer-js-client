const onEnter = async (config, machine) => {
    const body = JSON.stringify({
        grant_type: 'revoke_token',
        refresh_token: config.tokenStore.getRefreshToken(),
    });

    const response = await fetch(config.oauthState.get('token_endpoint'), {
        method: 'POST',
        mode: 'cors',
        body,
        credentials: 'include',
        headers: {
            accept: 'application/json',
            'content-type': 'application/json',
        },
    });

    if (response.status !== 200)
        throw new Error(
            `Logout failed due to unexpected response: ${response.status}`,
        );

    const data = await response.json();

    if (data.redirect_uri) {
        data.redirect_url = new URL(
            data.redirect_uri,
            config.oauthState.get('token_endpoint'),
        ).href;
    }

    config.tokenStore.clearTokens(data);
    return machine.advanceTo('UNAUTHENTICATED');
};

export default {
    name: 'LOGOUT',
    onEnter,
};
