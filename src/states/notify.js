import { getUrlParams } from '../utils';

const isSameOrigin = (window, parent) => {
    try {
        parent.location.href;
        return true;
    } catch (e) {
        return false;
    }
};

async function onEnter(clientConfig, machine) {
    const params = getUrlParams();
    if (!params.code) throw new Error('Ended up in CODE with no refreshToken');

    const parent = window.opener || window.parent;
    if (parent !== window && isSameOrigin(window, parent) && window.name === "auth") {
        const fnName = `completeOAuthLogin-${params.state}`;
        if (parent[fnName] === undefined) {
            const err = new Error(
                'Auth unable to complete - parent window state not found',
            );
            err.type = 'ParentWindowStateNotFound';
            throw err;
        }
        return parent[fnName](params.code);
    }

    clientConfig.tokenStore.setAuthorizationCode(getUrlParams().code);

    window.history.pushState({}, null, clientConfig.oauthState.get('route'));
    return machine.advanceTo('TOKEN_EXCHANGE');
}

export default {
    name: 'NOTIFY',
    onEnter,
};
