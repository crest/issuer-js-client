async function onEnter(clientConfig, machine) {
    const body = JSON.stringify({
        grant_type: 'refresh_token',
        refresh_token: clientConfig.tokenStore.getRefreshToken(),
    });

    const response = await fetch(
        clientConfig.oauthState.get('token_endpoint'),
        {
            method: 'POST',
            mode: 'cors',
            body,
            credentials: 'include',
            headers: {
                accept: 'application/json',
                'content-type': 'application/json',
            },
        },
    );

    const data = await response.json();

    if (response.status === 401) {
        clientConfig.tokenStore.invalidateRefreshToken();
        return machine.advanceTo('AUTHENTICATE');
    }
    if (response.status !== 200) {
        throw new Error(
            `Error during token refresh: ${data.error} : ${
                data.error_description
            }`,
        );
    }

    clientConfig.tokenStore.setAccessToken(data.access_token, data.expires_in);
    clientConfig.tokenStore.setRefreshToken(data.refresh_token);
    return machine.advanceTo('CONFIGURED');
}

export default {
    name: 'REFRESH',
    onEnter,
};
