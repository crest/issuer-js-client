async function onEnter(clientConfig, machine) {}

export default {
    name: 'CONFIGURED',
    onEnter,
    configured: true,
};
