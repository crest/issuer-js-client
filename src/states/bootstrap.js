async function onEnter(clientConfig, machine) {
    if (window.location.search.indexOf('code') > -1)
        return machine.advanceTo('NOTIFY');

    // Fetch all service configurations
    const configs = await Promise.all(
        clientConfig.services.map(async service => {
            if (service.endpoints) return service.endpoints;
            if (!service.issuerUrl) {
              throw new Error(`Must provide either an endpoints configuration or issuerUrl for service ${service.name}`)
            }
            const url = `${service.issuerUrl}/.well-known/openid-configuration`;
            return (clientConfig.openIdConfig = await fetch(url).then(
                response => {
                    if (response.status === 200) return response.json();

                    throw new Error(
                        `Unexpected response when fetching openId configuration for service '${
                            service.name
                        }': ${response.status}`,
                    );
                },
            ));
        }),
    );

    // Verify all services have the same authorization/token endpoints
    configs.forEach((ele, index) => {
        if (index === 0) return;
        const previousConfig = configs[index - 1];
        if (
            ele.authorization_endpoint !==
                previousConfig.authorization_endpoint ||
            ele.token_endpoint !== previousConfig.token_endpoint
        )
            throw new Error(
                'All openid-configuration endpoints must have the same authorization/token endpoints',
            );
    });

    clientConfig.oauthState.put(
        'authorization_endpoint',
        configs[0].authorization_endpoint,
    );
    clientConfig.oauthState.put('token_endpoint', configs[0].token_endpoint);

    console.log('OpenID config loaded', configs[0]);

    const nextState = clientConfig.autoRedirect ? 'REDIRECT' : 'AUTHENTICATE';
    return machine.advanceTo(nextState);
}

export default {
    name: 'BOOTSTRAP',
    onEnter,
};
