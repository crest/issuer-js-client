class TokenStore {
    constructor() {
        this.events = {};
        this.accessToken = null;
        this.refreshToken = null;
    }

    hasRefreshToken() {
        return this.getRefreshToken() !== null;
    }

    getRefreshToken() {
        return this.refreshToken;
    }

    hasAccessToken() {
        return this.accessToken !== null;
    }

    getAccessToken() {
        return this.accessToken;
    }

    setAccessToken(accessToken, expirationTime) {
        this.accessToken = accessToken;

        this.accessTokenExpiration = expirationTime
            ? Date.now() + expirationTime * 1000
            : null;

        this._broadcastEvent('accessToken.obtained');
    }

    setRefreshToken(refreshToken) {
        this.refreshToken = refreshToken;
    }

    invalidateRefreshToken(data) {
        this.refreshToken = null;
        this._broadcastEvent('refreshToken.invalidated', data);
    }

    clearTokens(data) {
        this.refreshToken = null;
        this.accessToken = null;
        this._broadcastEvent('tokens.invalidated', data);
    }

    setAuthorizationCode(code) {
        this.authorizationCode = code;
    }

    getAuthorizationCode() {
        return this.authorizationCode;
    }

    invalidateAccessToken() {
        this.accessToken = null;
        this._broadcastEvent('accessToken.invalidated');
    }

    getAccessTokenExpiration() {
        return this.accessTokenExpiration;
    }

    on(eventName, callback) {
        return this._registerEventHandler(eventName, { once: false, callback });
    }

    once(eventName, callback) {
        return this._registerEventHandler(eventName, { once: true, callback });
    }

    _broadcastEvent(eventName, info) {
        const handlers = this.events[eventName];
        if (handlers === undefined) return;

        this.events[eventName] = this.events[eventName].filter(e => !e.once);

        handlers.forEach(handler => {
            setTimeout(() => handler.callback(info));
        });
    }

    _registerEventHandler(eventName, eventInfo) {
        if (this.events[eventName] === undefined) this.events[eventName] = [];

        this.events[eventName].push(eventInfo);
        return () =>
            (this.events[eventName] = this.events[eventName].filter(
                e => e !== eventInfo,
            ));
    }
}

export default TokenStore;
