const SYMBOLS = [
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N',
    'O',
    'P',
    'Q',
    'R',
    'S',
    'T',
    'U',
    'V',
    'W',
    'X',
    'Y',
    'Z',
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'w',
    'x',
    'y',
    'z',
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '-',
    '.',
    '_',
    '~',
];

export default class PkceGenerator {
    static base64Encode(buffer) {
        return btoa(String.fromCharCode(...new Uint8Array(buffer)))
            .replace(/\+/g, '-')
            .replace(/\//g, '_');
    }

    async generateChallenge() {
        const randomData = new Uint32Array(Math.floor(Math.random() * 85) + 43);
        crypto.getRandomValues(randomData);

        const code = [...randomData]
            .map(d => SYMBOLS[d % SYMBOLS.length])
            .join('');

        const encodedCode = this.encode(code);
        const digest = await crypto.subtle.digest('SHA-256', encodedCode);

        return Object.freeze({
            code,
            challenge: PkceGenerator.base64Encode(digest),
            method: 'S256',
        });
    }

    encode(str) {
        if (window.TextEncoder) return new TextEncoder().encode(str);

        const utf8 = unescape(encodeURIComponent(str));
        const result = new Uint8Array(utf8.length);
        for (let i = 0; i < utf8.length; i++) {
            result[i] = utf8.charCodeAt(i);
        }
        return result;
    }
}
